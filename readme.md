**Base class for building client side of APIs in Delphi projects. **

**Uses**

1. **XSuperObject** https://github.com/onryldz/x-superobject 

2. **JsonableObject** https://bitbucket.org/vkrapotkin/jsonableobject 

3. **UnescapeJson** https://bitbucket.org/vkrapotkin/unescapejson/src 


Example of result API method procedure 

    TReqApi.ProjectUsers(const AProjectGuid: string); 
    begin 
        ClearParams; 
        RESTparams.Add('projects'); 
        RESTparams.Add(AProjectGuid); 
        RESTparams.Add('access'); 
        RESTparams.Add('users'); 
        RequestEx('GET'); 
    end; 

..... 

    Api.ProjectUsers(ProjectGuid); 
    if (not Api.Err.HasErrors) then 
    begin
      for i :=0 to Api.ResultJson.A['data'].length -1 do 
      begin 
        u := TRqdUser.CreateFromJSON(X); 
        flist.add(u); 
        FAccessRoles.Add(x['access_role'].AsString); 
      end;
    end;